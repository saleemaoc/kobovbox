#fix paths
## copy logo
#cp assets/kobologo.svg .vols/static/kpi/compiled/kobologo.svg
cp assets/DataToolBox_nobg.svg .vols/static/kpi/img/kobologo.svg

## copy bg
sudo cp assets/bg.png .vols/static/kpi/compiled

## replace bg in css
sed -i -e 's|url(/static/compiled/signup_photo\.jpg)|url(/static/compiled/bg\.png)|g' .vols/static/kpi/compiled/app.css 

## remove bottom icons, replace in file /root/kobo-docker/.vols/static/kpi/compiled/app-ade719972200e923825a.js the following text
sed -i -e 's|,u.default.createElement("div",{className:"k-drawer__icons-bottom"},k.default.session.currentAccount?u.default.createElement("a",{href:k.default.session.currentAccount.projects_url,className:"k-drawer__link",target:"_blank"},u.default.createElement("i",{className:"k-icon k-icon-globe"}),(0,N.t)("Projects (legacy)")):null,u.default.createElement("a",{href:"https://github.com/kobotoolbox/",className:"k-drawer__link",target:"_blank"},u.default.createElement("i",{className:"k-icon k-icon-github"}),(0,N.t)("source")),u.default.createElement("a",{href:"http://support.kobotoolbox.org/",className:"k-drawer__link",target:"_blank"},u.default.createElement("i",{className:"k-icon k-icon-help"}),(0,N.t)("help")))||g' .vols/static/kpi/compiled/app-*.js

# header bar bg color
sed -i -e "s|k-header__bar{background-color:#4d5e7b;|k-header__bar{background-color:rgba(0,0,0,0.65);|g" .vols/static/kpi/compiled/app.css
