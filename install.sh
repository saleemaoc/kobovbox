#docker
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list

sudo apt-get update
apt-cache policy docker-engine

#sudo apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual
sudo apt-get update
sudo apt-get install docker-engine
sudo service docker start
sudo docker run hello-world


#docker-compose
curl -L https://github.com/docker/compose/releases/download/1.9.0/docker-compose-`uname -s`-`uname -m` | sudo tee /usr/local/bin/docker-compose > /dev/null
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version

#prepare kobocat files

#git clone https://github.com/kobotoolbox/kobo-docker.git
#ln -s docker-compose.local.yml docker-compose.yml
#wget https://cf.checkform.net/static/compiled/kobofiles.tar.gz && tar -xvzf kobofiles.tar.gz && rm kobofiles.tar.gz

# copy scripts and files
#cp kobofiles/runtime_variables_kpi.source.bash ~/kobo-docker/scripts/
#cp kobofiles/docker-compose.local.yml ~/kobo-docker/
#cp kobofiles/envfile.local.txt ~/kobo-docker/
#ln -sf ~/kobo-docker/docker-compose.local.yml ~/kobo-docker/docker-compose.yml

git clone https://gitlab.com/saleemaoc/kobovbox.git

#build and run docker images
cd kobovbox && docker-compose pull
cd kobovbox && docker-compose build
cd kobovbox && docker-compose up -d

# post install tasks
#sh script.sh

