# replace title
sed -i -e 's|KoBoToolbox|CheckForm.net|g' /srv/src/kpi/kpi/templates/base.html

# email subject
echo "Activate your account | CheckForm.net" > /srv/src/kpi/kpi/templates/registration/activation_email_subject.txt

# email body
echo "You (or someone pretending to be you) have asked to register an account at CheckForm.net | Forms. If this wasn't you, please ignore this email and your address will be removed from our records. To activate this account, please click the following link within the next 3 days:

{{ kpi_protocol }}://{{ site.domain }}/accounts/activate/{{activation_key}}/

Sincerely, Check Forms| Forms Management"  > /srv/src/kpi/kpi/templates/registration/activation_email.txt

# registration form
#echo '{% extends "registration.html" %}
#{% load staticfiles %}
#
#{% block content %}
#<div class="registration__bg">
#
#  <form action="." method="post" class="registration registration--register">
#    <!--<div class="registration--logo"><a href="/"><img src="{% static img/kobologo.svg %}"/></a></div>-->
#
#    <div class="registration__first-half" style="width:100%;">
#      {% csrf_token%}
#      {{ form.as_p }}
#      <input type="submit" value="Create Account" class="registration__action" />
#      <div class="registration__footer">
#        or <a href="{% url auth_login %}">login</a>
#      </div>
#    </div>
#
#
#    <div style="clear:both;"></div>
#  </form>
#
#</div>
#
#<script>
#  $(function () {
#    $(form.registration input#id_username).attr(placeholder, Username);
#    $(form.registration input#id_email).attr(placeholder, Email);
#    $(form.registration input#id_password1).attr(placeholder, Password);
#    $(form.registration input#id_password2).attr(placeholder, Repeat Password);
#    $(form.registration p label).addClass(hidden);
#  });
#</script>
#
#{% endblock %}' > /srv/src/kpi/kpi/templates/registration/registration_form.html

# remove Kobo text on registration page
sed -i -e 's|<p>.*</p>||g'  /srv/src/kpi/kpi/templates/registration/registration_form.html

#make form 100% wide
sed -i -e 's|class="registration__first-half"|class="registration__first-half" style="width:100%;"|g'  /srv/src/kpi/kpi/templates/registration/registration_form.html

# registration complete test
sed -i -e 's|<p class="registration__message  registration__message--complete">Thanks for creating a KoBoToolbox account.</p>||g' /srv/src/kpi/kpi/templates/registration/registration_complete.html

# remove create account link
sed -i 's|or <a.*create an account</a>||g' templates/registration/login.html

# remove drawer icons
sed -i -e "s|<Drawer/>||g" staticfiles/js/app.es6

# delete gender
#sed -i "32,53d" /srv/src/kpi/kpi/forms.py
grep -q "    gender = " /srv/src/kpi/kpi/forms.py && sed -i "32,53d" /srv/src/kpi/kpi/forms.py
sed -i -e "s|'gender',||g" /srv/src/kpi/kpi/forms.py
sed -i -e "s|'sector',||g" /srv/src/kpi/kpi/forms.py
sed -i -e "s|'country',||g" /srv/src/kpi/kpi/forms.py

#sed -i -e 's|kobologo.svg|kobologo.png|g' /srv/src/kpi/kpi/**/**/*.html 

# replace kobologo.svg in registration pages with compiled/kobologo.svg instead of img/kobolog.svg
#ls -d -1 /srv/src/kpi/kpi/templates/registration/*.html | xargs sed -i -e "s|img/kobologo.svg|compiled/kobologo.svg|" 

#add recaptcha
#sed -i  -e 's|</script>|</script>\r\n<script src='https://www.google.com/recaptcha/api.js'></script>|g' /srv/src/kpi/kpi/templates/registration/registration_form.html
#sed -i -e 's|</form>|<div class="g-recaptcha" data-sitekey="6LdVpwsUAAAAAMocotWHeG31ehociQ1wfmDa_r-a"></div>\r\n</form>|g' /srv/src/kpi/kpi/templates/registration/registration_form.html

##django recaptcha
pip freeze  | grep -q django-recaptcha || pip install django-recaptcha
#grep -q "django-recaptcha" /srv/src/kpi/requirements.in && sed "s/django-recaptcha/django-recaptcha/" -i /srv/src/kpi/requirements.in || sed "$ a\django-recaptcha" -i /srv/src/kpi/requirements.in
#sed -i -e "s|'markitup',|'markitup',\r\n    'captcha',|g" /srv/src/kpi/kobo/settings.py
#sed -i "$ a\RECAPTCHA_PUBLIC_KEY = '6LdVpwsUAAAAAMocotWHeG31ehociQ1wfmDa_r-a'\r\nRECAPTCHA_PRIVATE_KEY = '6LdVpwsUAAAAAH2-uy8zpC_QzC3p81bwUP-A34FD'" /srv/src/kpi/kobo/settings.py
##sed -i "7 i\from captcha\.fields import ReCaptchaField#" /srv/src/kpi/kpi/forms.py
##sed -i "19 i\    captcha = ReCaptchaField()#" /srv/src/kpi/kpi/forms.py
#
#
#grep -q "from captcha\.fields import ReCaptchaField" /srv/src/kpi/kpi/forms.py && sed "s/from captcha\.fields import ReCaptchaField/from captcha\.fields import ReCaptchaField/" -i /srv/src/kpi/kpi/forms.py || sed "7 i\from captcha\.fields import ReCaptchaField" -i /srv/src/kpi/kpi/forms.py
#
#grep -q "    captcha = ReCaptchaField()" /srv/src/kpi/kpi/forms.py && sed "s/    captcha = ReCaptchaField()/    captcha = ReCaptchaField()/" -i /srv/src/kpi/kpi/forms.py || sed "19 i\    captcha = ReCaptchaField()" -i /srv/src/kpi/kpi/forms.py
#
#
#
##cat /srv/src/kpi/kobo/settings.py
#cat /srv/src/kpi/kpi/forms.py

#sed -i -e "0,/'captcha',/{s|'captcha',||g}" /srv/src/kpi/kobo/settings.py


mkdir -p /srv/src/kpi/kpi/templates/captcha

wget -P /srv/src/kpi/kpi/templates/captcha https://raw.githubusercontent.com/saleemaoc/django-recaptcha/develop/captcha/templates/captcha/widget.html
wget -P /srv/src/kpi/kpi/templates/captcha https://raw.githubusercontent.com/saleemaoc/django-recaptcha/develop/captcha/templates/captcha/widget_ajax.html
wget -P /srv/src/kpi/kpi/templates/captcha https://raw.githubusercontent.com/saleemaoc/django-recaptcha/develop/captcha/templates/captcha/widget_nocaptcha.html

# header logo inside app
wget https://gitlab.com/saleemaoc/kobovbox/raw/master/assets/kobologo.svg /srv/src/kpi/staticfiles/kobologo.svg

# logo on login page
#wget https://gitlab.com/saleemaoc/kobovbox/raw/master/assets/kobologo.svg /srv/src/kpi/static/img/kobologo.svg
cp /srv/src/kpi/staticfiles/kobologo.svg /srv/src/kpi/static/img/kobologo.svg

# sign up bg
wget https://gitlab.com/saleemaoc/kobovbox/raw/master/assets/bg.png /srv/src/kpi/staticfiles/img/signup_photo.jpg

if [[ ! -z "${PUBLIC_DOMAIN_NAME}" ]]; then
    export ENKETO_URL="https://${ENKETO_EXPRESS_PUBLIC_SUBDOMAIN}.${PUBLIC_DOMAIN_NAME}"
    export KOBOCAT_URL="https://${KOBOCAT_PUBLIC_SUBDOMAIN}.${PUBLIC_DOMAIN_NAME}"
    export KOBOCAT_INTERNAL_URL="${KOBOCAT_URL}" # FIXME: Use an actual internal URL.
    #export CSRF_COOKIE_DOMAIN=".${PUBLIC_DOMAIN_NAME}"
    #export DJANGO_ALLOWED_HOSTS=".${PUBLIC_DOMAIN_NAME}"

elif [[ ! -z "${HOST_ADDRESS}" ]]; then
    # Local configuration
    export ENKETO_URL="http://${HOST_ADDRESS}:${ENKETO_EXPRESS_PUBLIC_PORT}"
    export KOBOCAT_URL="http://${HOST_ADDRESS}:${KOBOCAT_PUBLIC_PORT}"
    export KOBOCAT_INTERNAL_URL="${KOBOCAT_URL}" # FIXME: Use an actual internal URL.
    #export CSRF_COOKIE_DOMAIN="${HOST_ADDRESS}"
    #export DJANGO_ALLOWED_HOSTS="${HOST_ADDRESS}"
else
    echo 'Please fill out your `envfile`!'
    exit 1
fi

export DJANGO_DEBUG="${KPI_DJANGO_DEBUG}"
export DJANGO_DEBUG=True
