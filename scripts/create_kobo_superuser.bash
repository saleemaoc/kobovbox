#!/bin/bash
set -e

source /etc/profile

cd /srv/src/kpi

##django recaptcha
sed -i -e "s|'markitup',|'markitup',\r\n    'captcha',|g" /srv/src/kpi/kobo/settings.py
sed -i "$ a\RECAPTCHA_PUBLIC_KEY = '6LdVpwsUAAAAAMocotWHeG31ehociQ1wfmDa_r-a'\r\nRECAPTCHA_PRIVATE_KEY = '6LdVpwsUAAAAAH2-uy8zpC_QzC3p81bwUP-A34FD'\r\nNOCAPTCHA = True" /srv/src/kpi/kobo/settings.py

grep -q "from captcha\.fields import ReCaptchaField" /srv/src/kpi/kpi/forms.py && sed "s/from captcha\.fields import ReCaptchaField/from captcha\.fields import ReCaptchaField/" -i /srv/src/kpi/kpi/forms.py || sed "7 i\from captcha\.fields import ReCaptchaField" -i /srv/src/kpi/kpi/forms.py

# uncomment this to add recaptcha field to registration form
#grep -q "    captcha = ReCaptchaField()" /srv/src/kpi/kpi/forms.py && sed "s/    captcha = ReCaptchaField()/    captcha = ReCaptchaField()/" -i /srv/src/kpi/kpi/forms.py || sed "19 i\    captcha = ReCaptchaField()" -i /srv/src/kpi/kpi/forms.py

sed -i -e "0,/'captcha',/{s|'captcha',||g}" /srv/src/kpi/kobo/settings.py


# This super long one-liner is to avoid issues with interactive python.
echo "import os; from django.contrib.auth.models import User; print 'UserExists' if User.objects.filter(username=os.environ['KOBO_SUPERUSER_USERNAME']).count() > 0 else User.objects.create_superuser(os.environ['KOBO_SUPERUSER_USERNAME'], 'kobo@example.com', os.environ['KOBO_SUPERUSER_PASSWORD'])" \
    | python manage.py shell --plain 2>&1
